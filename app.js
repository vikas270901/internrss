const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
var mongo = require("mongoose");
var model = require("./models/model");

app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs')

var url = process.env.DATABASEURL || "mongodb://localhost:27017/post11";
mongo.connect(url, { useNewUrlParser: true });

app.get('/', (req, res)=>{
    res.render('main');
});

app.get('/form', (req, res)=>{
    res.render('form');
});

app.post('/form', (req, res)=>{
 	model.create(req.body, function(err, resll){
	if(err) throw err;
		console.log(resll);
	    res.render('datasave', {data: req.body});
		//res.render("redirect", {ide:req.params.id}); 
	});
});

app.get('/fetchall', (req, res)=>{
 	model.find({}, function(err, resll){
	if(err) throw err;
		console.log(resll);
	    res.render('fetchall', {data: resll});
		//res.render("redirect", {ide:req.params.id}); 
	});
});

var PORT =process.env.PORT || 3000;
app.listen(3000, function(){
    console.log('Running on Port 3000...')
});
